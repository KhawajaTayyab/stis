class Banners < ActiveRecord::Base
	validates :title, presence: true, length: {minimum: 5, maximum: 25}
	validates :descriptions, presence: true, length: {minimum: 5}
end
