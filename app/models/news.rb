class News < ActiveRecord::Base
	validates :newsTitle, presence: true, length: {minimum: 5}
	validates :text, presence: true, length: {minimum: 5}
end
