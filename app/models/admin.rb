class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
	
	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

	#validation

	validates :firstName, presence: true, format: { with: /\A[a-zA-Z]+\z/, message: "Allows Letters" }, length: {minimum: 3, maximum: 25,}
	#validates :lastName, presence: true, format: { with: /\A[a-zA-Z]+\z/, message: "Allows Letters" }, length: {minimum: 3, maximum: 25,}
  	#validates :userName, presence: true, uniqueness: true, format: { with: /\A[a-zA-Z]+\z/, message: "Allows Letters" }, length: {minimum: 5, maximum: 25,}
    #validates :country, presence: true, format: { with: /\A[a-zA-Z]+\z/, message: "Allow Letters" }   
    #validates :city, presence: true, format: { with: /\A[a-zA-Z]+\z/, message: "Allow Letters" }
	#validates :address, presence: true, format: { with: /\A^[a-zA-Z\d ]+$\Z/i, message: "Allows Letters and Numbers" }
	#validates :organization, format: { with: /\A[a-zA-Z]+\z/, message: "Allow Letters" }, :allow_blank => true 
	#validates :postleCode, presence: true, numericality: true
	#validates :contactNumber, presence: true, numericality: true

end
