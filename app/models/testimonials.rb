class Testimonials < ActiveRecord::Base
	validates :clintsName, presence: true, length: {minimum: 5, maximun: 25}
	validates :clintsDescriptions, presence: true, length: {minimum: 5}
end
