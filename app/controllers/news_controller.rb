class NewsController < ApplicationController

	def new
		@news = News.new
	end

	def edit
		@editNews = News.find(params[:id])
	end

	def update
		@editNews = News.find(params[:id]) 
		if @editNews.update(params[:news].permit(:newsTitle,:text, :sortNo))
			redirect_to news_index_path
		else
	 		render 'edit'
		end
	end

	def destroy
		@news = News.find(params[:id])
  		@news.destroy
 
 		redirect_to news_index_path
	end

	def index
		@news = News.all
	end

	def create
		#render text: params[:post].inspect
		@news = News.new(news_params)
		if	@news.save
			redirect_to news_index_path
		else
			render 'new'
		end
	end

	def show
		@news = News.find(params[:id])
	end

	private

	def news_params
    	params.require(:news).permit(:newsTitle, :text, :sortNo)
  	end

end
