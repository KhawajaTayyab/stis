class BannersController < ApplicationController

	def new
		@banners = Banners.new
	end

	def createBanners
		@banners = Banners.new(params_banners)
		if @banners.save
			redirect_to banners_index_path
		else
			render "new"
		end

	end

	def editBanners
		@editBanners = Banners.find(params[:id])
	end

	def updateBanners
		@editBanners = Banners.find(params[:id])
		if @editBanners.update(params[:banners].permit(:title, :descriptions))
			redirect_to banners_index_path	
		else
			render "editBanners"
		end	
	end

	def destroy
		@banners = Banners.find(params[:id])
		@banners.delete

		redirect_to banners_index_path
	end

	def show
		@banners = Banners.find(params[:id])
	end

	def index
		@banners = Banners.all	
	end

	private
	def params_banners
		params.require(:banners).permit(:title, :descriptions)
	end
end
