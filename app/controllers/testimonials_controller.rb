class TestimonialsController < ApplicationController

	def new
		@testimonials = Testimonials.new
	end

	def createTestimonials
		@testimonials = Testimonials.new(params_testimonials)
		if @testimonials.save
			redirect_to testimonials_index_path
		else
			render "new"
		end
	end

	def updateTestimonials
		@editTestimonials = Testimonials.find(params[:id])
		if @editTestimonials.update(params[:testimonials].permit(:clintsName, :clintsDescriptions))
			redirect_to testimonials_index_path
		else
			render "editTestimonials"
		end
	end

	def editTestimonials
		@editTestimonials = Testimonials.find(params[:id])
	end
	def index
		@testimonials = Testimonials.all
	end

	def destroy
		@testimonials = Testimonials.find(params[:id])
		@testimonials.delete
		redirect_to testimonials_index_path		
	end

	private

	def params_testimonials
		params.require(:testimonials).permit(:clintsName, :clintsDescriptions)
	end

end
