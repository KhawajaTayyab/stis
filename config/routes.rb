Stis::Application.routes.draw do

  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
  devise_for :admins, :controllers => { :registrations => "admin/registrations", :sessions => "admin/sessions", :passwords => "admin/passwords" }
  
  # Root Page Main Page
  get "welcome/index"
  root "welcome#index"

  # Rails_Admins


  # News Controller
  
  get "news/new", :to => "news#new"
  get "news/index", :to => "news#index"
  post "news/create"
  get "news/edit/:id", :to => "news#edit", as: :news_edit
  post "news/update/:id", :to => "news#update", as: :news_update
  delete "news/destroy/:id", :to => "news#destroy", as: :news_destroy


  # Banners Controller

  get "banners/new", :to => "banners#new"
  get "banners/index", :to => "banners#index"
  post "banners/createBanners"
  get "banners/editBanners/:id", :to => "banners#editBanners", as: :banners_editBanners
  post "banners/updateBanners/:id", :to => "banners#updateBanners", as: :banners_updateBanners
  delete "banners/destroy/:id", :to => "banners#destroy", as: :banners_destroy


  # Testimonial Controller for Clints Info

  get "testimonials/new", :to => "testimonials#new"
  get "testimonials/index", :to => "testimonials#index"
  get "testimonials/editTestimonials/:id", :to => "testimonials#editTestimonials", as: :testimonials_editTestimonials
  post "testimonials/updateTestimonials/:id", :to => "testimonials#updateTestimonials", as: :testimonials_updateTestimonials
  post "testimonials/createTestimonials"
  delete "testimonials/destroy/:id", :to => "testimonials#destroy", as: :testimonials_destroy
  
  # routes for Devise
  devise_scope :admin do
  end



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
