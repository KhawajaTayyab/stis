class AddNewsTitleToNews < ActiveRecord::Migration
  def up
  	add_column :news, :newsTitle, :string
  end
  def down
  	remove_column :news, :newsTitle, :string
  end
end
