class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|

    	t.string :title
    	t.string :descriptions
      t.timestamps
    end
  end
end
