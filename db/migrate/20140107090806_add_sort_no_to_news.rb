class AddSortNoToNews < ActiveRecord::Migration
  def up
  	add_column :news, :sortNo, :integer
  end

  def down
  	remove_column :news, :sortNo, :integer
  end

end
